%global libdap_tag 3.21.0-27
Name:          libdap
Version:       3.21.0.27
Release:       2
Summary:       The C++ DAP2 and DAP4 library from OPeNDAP.
License:       LGPLv2+
URL:           http://www.opendap.org/
Source0:       https://github.com/OPENDAP/libdap4/archive/%{libdap_tag}/%{name}-%{version}.tar.gz

# Remark HTTP tests - builders don't have network connections
Patch0:        0001-libdap-offline.patch

BuildRequires: make
BuildRequires: gcc-c++
# For autoreconf
BuildRequires: libtool
BuildRequires: bison >= 3.0
BuildRequires: cppunit-devel
BuildRequires: curl-devel
BuildRequires: doxygen
BuildRequires: flex
BuildRequires: graphviz
BuildRequires: libtirpc-devel
BuildRequires: libuuid-devel
BuildRequires: libxml2-devel
BuildRequires: openssl-devel
BuildRequires: pkgconfig
%ifarch %{valgrind_arches}
BuildRequires: valgrind
%endif
Provides:      bundled(gnulib)

%description
A C++ SDK which contains an implemention of DAP2.0 and DAP4.0.This includes both Client-side and
Server-side support classes.

%package       devel
Summary:       Development and header files from libdap
Requires:      %{name} = %{version}-%{release} automake curl-devel libxml2-devel pkgconfig

%description   devel
The package provides documents for applications which developed with %{name}.

%package       help
Summary:       Help documents for %{name}
Provides:      %{name}-doc = %{version}-%{release}
Obsoletes:     %{name}-doc < %{version}-%{release}

%description   help
Man pages and other help documents for %{name}.

%prep
%autosetup -n libdap4-%{libdap_tag} -p1

%build
autoreconf -f -i
%configure --disable-dependency-tracking --disable-static
%make_build
make docs

%install
%make_install INSTALL="%{__install} -p"
mkdir -p $RPM_BUILD_ROOT%{_libdir}/libdap
mv $RPM_BUILD_ROOT%{_libdir}/libtest-types.a $RPM_BUILD_ROOT%{_libdir}/libdap/
mv $RPM_BUILD_ROOT%{_bindir}/dap-config-pkgconfig $RPM_BUILD_ROOT%{_bindir}/dap-config
rm -rf __dist_docs
cp -pr html __dist_docs
rm -f __dist_docs/*.map __dist_docs/*.md5
touch -r ChangeLog __dist_docs/*

%check
make check || :

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYRIGHT_W3C COPYING COPYRIGHT_URI
%{_bindir}/getdap*
%{_libdir}/*.so.*
%exclude %{_libdir}/*.la

%files devel
%{_bindir}/dap-config
%{_libdir}/*.so
%{_libdir}/libdap/
%{_libdir}/pkgconfig/libdap*.pc
%{_includedir}/libdap/
%{_datadir}/aclocal/*

%files help
%doc README.md README.dodsrc NEWS
%doc __dist_docs/
%{_mandir}/man1/*

%changelog
* Fri Feb 14 2025 laokz <zhangkai@iscas.ac.cn> - 3.21.0.27-2
- let BuildRequire valgrind depend on system %{valgrind_arches}

* Mon Mar 11 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 3.21.0.27-1
- Update package to version 3.21.0.27
  Added libdap::Array::rename_dim() for HK-247
  Added new Direct I/O support so that modules written using libdap can pass compressed data buffers
  Fix handling of libtirpc pkg-config files with -L flags
  Moved the functionality of is_dap4_projected() into libdap4
  Added support for 64-bit sized arrays

* Fri Mar 6 2020 zhouyihang<zhouyihang1@huawei.com> - 3.19.1-3
- Pakcage init
